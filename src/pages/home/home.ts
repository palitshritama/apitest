import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NetworkEngineProvider } from '../../providers/network-engine/network-engine';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
   
      result: any = [];
	  data: Observable<any>;

  constructor(public navCtrl: NavController, public http: HttpClient) {

  }
   getData(){
       var url = 'https://stage.noargs.com/appi/users';
	   this.data = this.http.get(url);
	   this.data.subscribe(data =>{
	     this.result = data;
		 });
}
     
	 postData(){
       var url = 'https://stage.noargs.com/appi/users';
	   let postData = new FormData();
	   postData.append('key','value');
	   this.data = this.http.post(url);
	   this.data.subscribe(data =>{
	     console.log(data);
		 form.reset();
		 });
 }
 
 }

